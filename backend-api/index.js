const express = require('express')
const app = express()
const os = require('os')

app.get('/', (req, res) =>  {
    let item;
	if (process.env.NODE_ENV === 'production') {
		item = {
			Environment: 'Production',
			Hostname: os.hostname(),
			UserInfo: os.userInfo(),
			OS: {
				type: os.type(),
				platform: os.platform(),
				architecture: os.arch()
			},
		}
	} else {
		item = {
			Environment: 'Development etc',
			Hostname: os.hostname(),
			UserInfo: os.userInfo(),
			OS: {
				type: os.type(),
				platform: os.platform(),
				architecture: os.arch()
			},
			Networks: os.networkInterfaces()
		}
	}
	res.json(item)
})

app.listen(3000, err => {
    if (err) throw err

    console.log('server is listening port: http://localhost:' + 3000);
  })
